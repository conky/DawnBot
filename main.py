'''
Created on 18 Feb 2018

@author: Conky
'''
import discord, asyncio, logging

from discord.ext import commands
from discord import reaction

bot = commands.Bot(command_prefix="d!", description="Custom bot for Dawn which applies a role to a user who has agreed to the rules")

@bot.event
async def on_raw_reaction_add(emoji, message_id, channel_id, user_id):
    server = bot.get_channel(channel_id).guild
    if message_id == 414819882379640833:
        if emoji.name == "\u2611":
            # give the user the role
            role = discord.utils.get(server.roles, name='Agreed to rules')
            user = server.get_member(user_id)
            print ("Assigning agreed to rules role to {}".format(user.name))
            await user.add_roles(role, reason="Agreed to rules")
                
@bot.event
async def on_raw_reaction_remove(emoji, message_id, channel_id, user_id):
   server = bot.get_channel(channel_id).guild
   if message_id == 414819882379640833:
        if emoji.name == "\u2611":
            # give the user the role
            role = discord.utils.get(server.roles, name='Agreed to rules')
            user = server.get_member(user_id)
            print("Removing agreed to rules role from {}".format(user.name))
            await user.remove_roles(role, reason="Disagreed to rules")

async def LoadCogs():
    try:
        for cog in startupExtensions:
            bot.load_extension(cog)
            print("{} loaded".format(cog))
    except ImportError as e:
        print ("Unable to load cog.")
        
        
def Main():
    logging.basicConfig(level=logging.INFO)
    bot.run("")
        
if __name__ == '__main__':
    Main()
